﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Matrix_class
{
    public class Matrix
    {
        private int n_str; //количество строк
        private int n_col; //количество столбцов
        public int[,] matrix; // обрабатываемая матрица

        public Matrix(int matrixSzie)
        {
            n_str = matrixSzie; // на входе матрица квадратная
            n_col = matrixSzie;
            matrix = new int[n_str, n_col];
        }

        //автозаполнение матрицы 
        public void RandomInput()
        {
            Random rnd = new Random();
            //  0-нет дороги
            for (int i = 0; i < n_str; i++)
                for (int j = 0; j < n_col; j++)
                {   
                    matrix[i, j] = 0; //чтобы работать с одной половиной
                    matrix[j, i] = rnd.Next(30);
                    matrix[i, i] = 0; 
                }            
        }

        //заполнение матрицы из DataGridView
        public void GridToMatrix(DataGridView Grid)
        {   
            DataGridViewCell Cell;
            for (int i = 0; i < n_str; i++)
            {
                for (int j = 0; j < n_col; j++)
                {
                    Cell = Grid.Rows[i].Cells[j];
                    string s = Cell.Value.ToString();
                    if (s == "")
                    {
                        matrix[i, j] = 0;                     
                    }
                    else
                    {
                        matrix[i, j] = Int32.Parse(s);
                    }
                }
            }
        }
        //заполнение матрицы из DataGridView для удобства работы с матрицей

        public void vtor_pol(DataGridView Grid)
        {
            for (int i = 0; i < n_str; i++)
                for (int j = 0; j < n_col; j++)
                {
                    matrix[j, i] =matrix[i, j];
                    matrix[i, i] = 0;
                }
        }

        //вывод матрицы в DataGridView
        public void MatrixToGrid(DataGridView Grid)
        {
            //установка размеров
            int i;
            DataTable matr = new DataTable("matrix");
            DataColumn[] cols = new DataColumn[n_col];

            for (i = 0; i < n_col; i++)
            {
                cols[i] = new DataColumn(i.ToString());
                matr.Columns.Add(cols[i]);
            }

            for (i = 0; i < n_str; i++)
            {
                DataRow newRow;
                newRow = matr.NewRow();
                matr.Rows.Add(newRow);
            }
            Grid.DataSource = matr;

            for (i = 0; i < n_col; i++)
                Grid.Columns[i].Width = 50;

            // занесение значений
            DataGridViewCell Cell;
            for (i = 0; i < n_str; i++)
            {
                for (int j = 0; j < n_col; j++)
                {
                    Cell = Grid.Rows[i].Cells[j];
                    Grid.Rows[i].HeaderCell.Value = i.ToString();
                    Cell.Value = matrix[i, j];
                    Grid.Rows[i].Cells[i].ReadOnly = true;
                    Grid.Rows[i].Cells[i].Style.BackColor = Color.Red;
                    if (i>j)
                    {
                        Grid.Rows[i].Cells[j].ReadOnly = true;
                        Grid.Rows[i].Cells[j].Style.BackColor = Color.Black;
                    }
                }
            }

        }
        
        //проверка существования подъезда к городу
        public bool Check_col()
        {
            bool ok;
            int i, k, way;
            ok = true;
            k = n_col - 1;
            while ( k >= 0 && ok )
            {
                way = 0;
                for (i = 0; i < n_str; i++)
                {
                    way = matrix[i, k] + way;
                }
                if (way == 0)   
                {
                    return false;
                }
                k--;
            }
            return ok;
        }
        //сколько дорог ведут в город
        public bool Check_circle()
        {
            bool ok;
            int i, k, countroad;
            ok = true;
            k = n_col - 1;
            while (k >= 0 && ok)
            {
                countroad = 0;
                for (i = 0; i < n_str; i++)
                {
                    if (matrix[i, k] !=0 | matrix[k, i] !=0)
                    {
                        countroad++;
                    }
                }
                k--;
                if (countroad <2) //если <2 то нет решения
                {
                    ok = false;
                }
            }
            return ok;
        }
        
    }

}
