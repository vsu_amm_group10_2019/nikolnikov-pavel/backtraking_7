﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Matrix_class;

namespace Back_Tracking
{
    public partial class Form1 : Form
    {
        private int matrixSize;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_usl_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Задана система двусторонних дорог." +
                             "\nНайти замкнутый путь, проходящий через каждую вершину и длиной не более 100 км." , 
                              "Условие задачи",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            int size; 
            size = (int)count_city.Value;
            if (size < 3)
            {
                MessageBox.Show("Минимальное число городов: 3", "Осторожно",
                              MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                matrixSize = size;
                Matrix mtr = new Matrix(matrixSize);
                mtr.MatrixToGrid(DTG);
                btn_auto.Visible = true;
                btn_do.Visible = true;
                lab0.Visible = true;
            }
        }

        private void btn_auto_Click(object sender, EventArgs e)
        {       //автозаполнение
            Matrix mtr = new Matrix(matrixSize);
            mtr.RandomInput();
            mtr.MatrixToGrid(DTG);
        }

        private void btn_do_Click(object sender, EventArgs e)
        {
            Matrix mtr = new Matrix(matrixSize);
            mtr.GridToMatrix(DTG);
            mtr.vtor_pol(DTG);
            mtr.MatrixToGrid(DTG);
            if (!mtr.Check_col())
            {
                MessageBox.Show("Есть города, в которые не ведут дороги",
                              "Решение отсутствует",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
            else 
            {
                if (!mtr.Check_circle())
                {
                    MessageBox.Show("Система дорог не замкнута",
                              "Решение отсутствует",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("что-то есть", "Решение");
                    Solver solver = new Solver(mtr.matrix, matrixSize);                  
                    

                }
            }

        }
    }
}
