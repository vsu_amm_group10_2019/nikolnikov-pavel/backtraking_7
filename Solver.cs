﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Back_Tracking
{

    public class Solver
    {
        public int coun_city;//кол-во городов=кол-во дорог в ответе
        public List<int> cityvisit = new List<int>();//очередь посещений
        public List<int> tupik = new List<int>();//вспомогательный
        public int max_way = 100;//условие задачи, 100 км
        public int nachalo;//из какого города идем
        public List<int> way = new List<int>();
        public int glubina = 0;//сколько раз мы шли назад
        public int[,] matrway;
        public string s2 = "", s1 = "";
        public bool proverka=true;
        public Solver(int[,] matr, int n)//получаем матрицу
        {
            matrway = matr;
            coun_city = n;
            nachalo = n - 1;
            searchroad();

        }
        
        public int lenght_way(List<int> list)//считаем текущий путь
        {            
            int put = 0;           
            for (int k = 0; k <list.Count; k++)
            {
                put += list[k];
            }
            return put;         
        }
        public string spisok(List<int> list)
        {
            string str = "";
            foreach (int inte in list)
            {
                str +=Convert.ToString(inte)+"=";
            }
            return str;
        }


        public void searchroad()//сделать строковый для вывода ответа
        {
            string dor = "", dlin = "";
            int[] dorogi = new int[coun_city];
            road(nachalo, proverka);
            if (cityvisit.Count==6)//из последнего города в графе точно есть дороги 
            {
                for (int i = 0; i < coun_city; i++)
                {
                    dorogi[i] = cityvisit.Last();
                    cityvisit.Remove(cityvisit.Last());//удаляем последний
                    dor += "-" + Convert.ToString(dorogi[i]);
                }
                dlin = Convert.ToString(lenght_way(way));
                MessageBox.Show("Длина цикла - " + dlin + " км" + "\nЦикл " + nachalo + dor,
                    "Ответ",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        
        }
        
        public List<int> road(int col, bool proverka)
        {
            int i = 0;
            bool ok = false;
            if (proverka)
            {
                while (i < coun_city && cityvisit.Count < coun_city && lenght_way(way)<max_way)
                {//пока не конец строки, не все города пройдены, путь не привышает допустимый
                    if (matrway[col, i] != 0 && !cityvisit.Contains(i) && !tupik.Contains(i)) //если есть дорога в I, города I еще не было, I не тупик 
                    {

                        if (!cityvisit.Contains(col))//чтобы при возвращении в предыдущий город, не добавлять его снова в стек
                        {
                            cityvisit.Add(col);
                            tupik.Clear();
                            way.Insert(cityvisit.Count - 1, matrway[col, i]);
                            s1 = spisok(cityvisit);
                         //   MessageBox.Show("\nКМ: " + spisok(way) + "\nОбщий" + lenght_way(way));
                         //   MessageBox.Show("\nMаршрут: " + spisok(cityvisit));// +" путь "+ Convert.ToString(lenght_way(way)));
                                                                               //  way[cityvisit.Count-1] = matrway[col, i];//запоминаем длину дороги

                        }
                        ok = true;
                        if (tupik.Count > 0)
                        {
                            if (matrway[i, tupik.Last()] != 0)
                            {
                                tupik.Clear();
                            }
                        }
                        road(i, proverka);
                    }
                    i++;
                }

                if (!ok && (cityvisit.Count < coun_city - 1))//если нет дорог дальше
                {
                    if (cityvisit.Last() == col)
                    {
                        cityvisit.Remove(cityvisit.Last());//убираем тупиковый путь
                        way.Remove(way.Last());
                        tupik.Clear();
                    }
                    //tupik.Clear();
                    tupik.Add(col);//заносим во временный тупик 
                    s2 = spisok(tupik);
                  //  MessageBox.Show("\nТупики: " + spisok(tupik));
                    road(cityvisit.Last(), proverka);//идем от предыдущего искать новый путь    
                }

                if (cityvisit.Count == (coun_city - 1) && (matrway[col, nachalo] != 0) && !cityvisit.Contains(col)&& (matrway[cityvisit.Last(), nachalo]+ lenght_way(way)) <max_way)
                {//если пройдены все города кроме последнего, есть путь из последнего в начало, длина цикла меньше 100км
                    proverka = false;
                    cityvisit.Add(col);
                    way.Add(matrway[cityvisit.Last(), nachalo]);
                  //  MessageBox.Show("\nКМ: " + spisok(way)+"\nОбщий"+ lenght_way(way));
                  //  MessageBox.Show("\nMаршрут конец: " + spisok(cityvisit));
                }
            }
            return cityvisit;
        }
    }
}


