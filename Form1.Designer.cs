﻿
namespace Back_Tracking
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_usl = new System.Windows.Forms.Button();
            this.DTG = new System.Windows.Forms.DataGridView();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_auto = new System.Windows.Forms.Button();
            this.btn_do = new System.Windows.Forms.Button();
            this.count_city = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.lab0 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DTG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.count_city)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_usl
            // 
            this.btn_usl.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_usl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_usl.Location = new System.Drawing.Point(0, 0);
            this.btn_usl.Name = "btn_usl";
            this.btn_usl.Size = new System.Drawing.Size(84, 23);
            this.btn_usl.TabIndex = 0;
            this.btn_usl.Text = "Условие";
            this.btn_usl.UseVisualStyleBackColor = true;
            this.btn_usl.Click += new System.EventHandler(this.btn_usl_Click);
            // 
            // DTG
            // 
            this.DTG.AllowUserToAddRows = false;
            this.DTG.AllowUserToDeleteRows = false;
            this.DTG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DTG.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.DTG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DTG.Location = new System.Drawing.Point(0, 29);
            this.DTG.Name = "DTG";
            this.DTG.Size = new System.Drawing.Size(733, 414);
            this.DTG.TabIndex = 1;
            // 
            // btn_ok
            // 
            this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ok.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_ok.Location = new System.Drawing.Point(178, 463);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(57, 20);
            this.btn_ok.TabIndex = 2;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_auto
            // 
            this.btn_auto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_auto.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_auto.Location = new System.Drawing.Point(471, 456);
            this.btn_auto.Name = "btn_auto";
            this.btn_auto.Size = new System.Drawing.Size(122, 31);
            this.btn_auto.TabIndex = 3;
            this.btn_auto.Text = "Автозаполнение";
            this.btn_auto.UseVisualStyleBackColor = true;
            this.btn_auto.Visible = false;
            this.btn_auto.Click += new System.EventHandler(this.btn_auto_Click);
            // 
            // btn_do
            // 
            this.btn_do.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_do.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_do.Location = new System.Drawing.Point(599, 456);
            this.btn_do.Name = "btn_do";
            this.btn_do.Size = new System.Drawing.Size(122, 31);
            this.btn_do.TabIndex = 4;
            this.btn_do.Text = "Выполнить";
            this.btn_do.UseVisualStyleBackColor = true;
            this.btn_do.Visible = false;
            this.btn_do.Click += new System.EventHandler(this.btn_do_Click);
            // 
            // count_city
            // 
            this.count_city.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.count_city.Location = new System.Drawing.Point(128, 463);
            this.count_city.Name = "count_city";
            this.count_city.Size = new System.Drawing.Size(44, 20);
            this.count_city.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 463);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Кол-во городов:";
            // 
            // lab0
            // 
            this.lab0.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lab0.AutoSize = true;
            this.lab0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lab0.Location = new System.Drawing.Point(260, 3);
            this.lab0.Name = "lab0";
            this.lab0.Size = new System.Drawing.Size(196, 20);
            this.lab0.TabIndex = 7;
            this.lab0.Text = "Если 0, то дороги нет";
            this.lab0.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(733, 499);
            this.Controls.Add(this.lab0);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.count_city);
            this.Controls.Add(this.btn_do);
            this.Controls.Add(this.btn_auto);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.DTG);
            this.Controls.Add(this.btn_usl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(550, 300);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Задача 7";
            ((System.ComponentModel.ISupportInitialize)(this.DTG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.count_city)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_usl;
        private System.Windows.Forms.DataGridView DTG;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_auto;
        private System.Windows.Forms.Button btn_do;
        private System.Windows.Forms.NumericUpDown count_city;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lab0;
    }
}

